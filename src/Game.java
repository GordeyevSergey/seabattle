import java.util.ArrayList;
import java.io.*;
import java.util.regex.*;

/**
 * Класс организации игры содержащий метод для взаимодействия с игроком и метод инициализации корабля
 */
public class Game {
    /**
     * Метод, взаимодействующий с игроком
     *
     */
    public static void main(String args[]) throws IOException {
        Ship ship = new Ship();
        ship.setLineLocation(shipInitialization());
        int countOfShots = 0;
        String resultOfShot;
        System.out.println("*Игра <<Морской бой>>*");
        System.out.println("Введите координаты выстрела");

        while ((resultOfShot = ship.checkShip(inputShot())) != "Потоплен") {
            countOfShots++;
            System.out.println(resultOfShot + "  Введите координаты выстрела еще раз");
        }

        System.out.println("Вы потопили корабль противника! \n Попыток использовано - " + countOfShots);
    }

    /**
     * Метод инициализации корабля
     *
     */
    private static ArrayList shipInitialization() {
        ArrayList location = new ArrayList();
        int firstNum = (int) (Math.random() * 5);
        for (int i = 0; i < 3; i++) {
            location.add(Integer.toString(firstNum + i));
        }
        return location;
    }

    /**
     * Метод ввода координат выстрела с клавиатуры
     *
     * @return координаты выстрела
     */
    private static String inputShot() throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        String shot;
        do{
            shot = bufferedReader.readLine();
        }while (isCheckShot(shot) != true);
        return shot;
    }

    private static boolean isCheckShot(String shot){
        Pattern pattern = Pattern.compile("[0-6]");
        Matcher matcher = pattern.matcher(shot);
        return matcher.matches();
    }
}
