import java.util.ArrayList;

/**
 *  Класс, содержащий параметры корабля ,содержащий метод проверки его состояния
 */
public class Ship {
    private ArrayList<String> lineLocation;

    /**
     * @param lineLocation ArrayList, содержащий координаты корабля
     */
    public void setLineLocation(ArrayList<String> lineLocation) {
        this.lineLocation = lineLocation;
    }

    /**
     *  Метод, определяющий состояние корабля
     * @param shot Координата выстрела
     * @return Строка, содержащая "Попал", если координаты выстрела совпали хоть с одной из координат корабля,
     * "Мимо", если координата выстрела не совпала ни с одной координатой корабля,
     * "Потоплен", если ArrayList, содержащий координаты корабля, окажется пустым
     */
    public String checkShip(String shot) {
        String result = "Мимо";
        int index;
        if ((index = lineLocation.indexOf(shot)) > (-1)) {
            lineLocation.remove(index);
            result = "Попал";
        }

        if (lineLocation.isEmpty()) {
            result = "Потоплен";
        }
        return result;
    }

    /**
     * Метод, выводящий на экран параметры корабля
     * @return Строка, содержащая координаты корабля
     */
    @Override
    public String toString() {
        return "Ship{" +
                "lineLocation=" + lineLocation +
                '}';
    }
}
