Проект "Морской бой"     
     
Описание: Консольное приложение, взаимодействующее с пользователем-игроком посредством консоли
     
Содержание: Game.java , Ship.java     
     
Функциональность:     
Game.java     
1) Ввод с клавиатуры координаты выстрела      
2) Инициализация корабля      
3) Вывод на экран оповещений о ходе игры      
      
Ship.java      
1) Проверка состояния корабля    